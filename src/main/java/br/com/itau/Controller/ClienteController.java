package br.com.itau.Controller;


import br.com.itau.ClienteNaoEncontrado;
import br.com.itau.Model.Cliente;
import br.com.itau.Service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
//@RequestMapping("/clientes")
public class ClienteController {


    @Autowired
    ClienteService clienteService;


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente cadastrarCliente(@RequestBody @Valid Cliente cliente) {

        return clienteService.cadastrarnovocliente(cliente);

    }

    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Cliente buscarClientePeloId(@PathVariable(name = "id") int id) {
            return clienteService.buscarClientePeloId(id);


    }

}